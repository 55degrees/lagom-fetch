import PayloadRegister from "./PayloadRegister";
import {requestJira, requestConfluence} from '@forge/bridge'

function getAccountId(applyAccountId, url, cb) {
  window.AP.getCurrentUser((user) => {
    if (user && user.atlassianAccountId) {
      if (url.indexOf("?") > 0) {
        cb(
          url +
            "&" +
            applyAccountId +
            "=" +
            encodeURIComponent(user.atlassianAccountId)
        );
      } else {
        cb(
          url +
            "?" +
            applyAccountId +
            "=" +
            encodeURIComponent(user.atlassianAccountId)
        );
      }
    }
  });
}

function getSuccess(cb) {
  return function (resp) {
    if (resp) {
		if( typeof resp === 'string')
		{
      		cb(null, JSON.parse(resp));
		}
		else
		{
			cb( null, resp);
		}
    } else {
      cb(null);
    }
  };
}

function getError(cb) {
  return function (xhr, _statusText, errorThrown) {
    console.log("ERROR: ", xhr, _statusText, errorThrown);
    if (errorThrown) {
      var obj = errorThrown;
      try {
        // This is also suspect...
        obj = JSON.parse(errorThrown);
      } catch {} // eat the error.

      cb({ status: xhr.status, error: obj });
    } else {
      cb({ status: xhr.status });
    }
  };
}

function getReqObj(method, url, data, opts, parentCb, cb) {
  const hasAJS = !!window.AJS;
  const hasAP = !!window.AP;

  const reqObj = {
    error: getError(parentCb),
    success: getSuccess(parentCb),
    type: method,
    url: hasAJS ? window.AJS.contextPath() + url : url,
  };

  if (method === "PUT" || method === "POST") {
    reqObj.contentType = "application/json";
    reqObj.data = JSON.stringify(data);
    if (hasAJS) {
      reqObj.dataType = "json";
    }
  }

  // Only do the account id check for Connect land.
  if (hasAP && opts.applyAccountId) {
    getAccountId(opts.applyAccountId, reqObj.url, (url) => {
      reqObj.url = url;
      cb(reqObj);
    });
  } else {
    cb(reqObj);
  }
}

function doRequest(method, url, data, opts, cb) {
  PayloadRegister.checkForPayload(
    method,
    url,
    data,
    async (check_for_preregistered_payload) => {
      if (check_for_preregistered_payload) {
        cb(null, check_for_preregistered_payload);
      } else if (window.AP || window.AJS) {
        getReqObj(method, url, data, opts, cb, (reqObj) => {
          if (window.AP) {
            window.AP.request(reqObj);
          } else {
            window.AJS.$.ajax(reqObj);
          }
        });
	  }
	  else if (opts.target && (opts.target==='confluence' || opts.target==='jira'))
	  {
		  var request_options = {
			method: method,
			headers: {'Content-Type':'application/json'},
			
		  };
		  if( data) { request_options.body = JSON.stringify( data)}
		try
		{
			var response;
			if( opts.target==='confluence')
			{
				response = await requestConfluence(url, request_options)
			}
			else if ( opts.target==='jira')
			{
				response = await requestJira(url, request_options);
			}
			
			if( response && response.status<300)
			{
				cb(null,  response.body )
			}
			else
			{
				cb(null, { status: response.status });

			}
		}
		catch(error)
		{
			cb(null, { status: 404 });

		}
	  }
	  else {
        cb(null, { status: 404 });
      }
    }
  );
}

export default function (method, url, data, opts, cb) {
  if (!cb && (typeof opts === 'function')) {
    cb = opts;
    if (!opts) {
      opts = {};
    }
  }
  if (!opts) {
    opts = {};
  }

  if (cb) {
    doRequest(method, url, data, opts, cb);
  } else {
    return new Promise((resolve, reject) => {
      doRequest(method, url, data, opts, (error, data) => {
        if (error) {
          reject(error);
        } else {
          resolve(data);
        }
      });
    });
  }
}
