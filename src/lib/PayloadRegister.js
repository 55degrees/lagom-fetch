var _lagom_payload_data = {
  payloads: null,
  disable: true,
  delay: 0,
  mode: null,
};

const PayloadRegister = {
  enable: (flag) => {
    _lagom_payload_data.disable = !flag;
  },

  setPayloadMode: (mode) => {
    _lagom_payload_data.mode = mode;
  },

  setRequestDelay: (delay) => {
    _lagom_payload_data.delay = delay;
  },
  registerBatchPayloads: (payloadHash) => {
    Object.values(payloadHash).forEach((payload) => {
      PayloadRegister.registerPayload(
        payload.method,
        payload.url,
        payload.body,
        payload.response,
        payload.mode
      );
    });
  },

  registerPayload: (method, url, body_object, response_object, mode) => {
    if (_lagom_payload_data.disable) {
      return;
    }

    if (_lagom_payload_data.payloads === null) {
      _lagom_payload_data.payloads = {};
    }

    var lookup_key = [method, url].join(":").toLowerCase();
    if (mode) {
      lookup_key = mode + ":" + lookup_key;
    }

    if (!body_object) {
      body_object = {};
    }
    if (!_lagom_payload_data.payloads[lookup_key]) {
      _lagom_payload_data.payloads[lookup_key] = [];
    }

    _lagom_payload_data.payloads[lookup_key].push({
      body: body_object,
      response: response_object,
    });
  },

  checkForPayload: (method, url, body_object, cb) => {
    if (_lagom_payload_data.disable) {
      cb(null);
    } else {
      const lookup_keys = [(method + ":" + url).toLowerCase()];
      if (_lagom_payload_data.mode) {
        lookup_keys.push(
          _lagom_payload_data.mode + ":" + (method + ":" + url).toLowerCase()
        );
      }

      var the_return_payload = null;
      if (!body_object) {
        body_object = {};
      }

      lookup_keys.forEach((lookup_key) => {
        const payloads = _lagom_payload_data.payloads[lookup_key];

        if (payloads && payloads.length > 0) {
          var found_response = null;
          payloads.forEach((p) => {
            var is_valid_payload = true;
            if (p.body) {
              Object.keys(p.body).forEach((required_body_key) => {
                if (
                  !body_object[required_body_key] ||
                  body_object[required_body_key] !== p.body[required_body_key]
                ) {
                  is_valid_payload = false;
                }
              });
            }
            if (is_valid_payload) {
              found_response = p.response;
            }
          });
          if (found_response) {
            the_return_payload = found_response;
          }
        }
      });

      if (the_return_payload) {
        if (_lagom_payload_data.delay) {
          window.setTimeout(() => {
            cb(the_return_payload);
          }, _lagom_payload_data.delay);
        } else {
          cb(the_return_payload);
        }
      } else {
        console.log(
          "not able to find registered payload " + method + " " + url
        );

        if (window && _lagom_payload_data.delay) {
          window.setTimeout(() => {
            cb(null);
          }, _lagom_payload_data.delay);
        } else {
          cb(null);
        }
      }
    }
  },
};

Object.freeze(PayloadRegister);
export default PayloadRegister;
