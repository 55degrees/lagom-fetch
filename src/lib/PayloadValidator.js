import lagomFetch from "./LagomFetch";

export default function (files) {
  var payloads = {};

  //merge all of the payloads
  files.forEach(async (file) => {
    var data = await (await fetch(file)).json();

    Object.keys(data).forEach((payload_key) => {
      payloads[payload_key] = data[payload_key];
    });
  });

  var payload_keys = Object.keys(payloads);
  payload_keys.sort();

  const start = async () => {
    var is_valid = true;

    await asyncForEach(payload_keys, async (key) => {
      var payload = payloads[key];
      const returned_data = await lagomFetch(
        payload.method,
        payload.url,
        payload.body
      );

      if (payload.verify) {
        is_valid = payload.verify(returned_data);
      } else {
        is_valid =
          JSON.stringify(payload.response) === JSON.stringify(returned_data);
      }
    });

    return is_valid;
  };

  return start();
}
