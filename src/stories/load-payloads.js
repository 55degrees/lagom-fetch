import React from "react";

import PayloadRegister from "../lib/PayloadRegister";
import lagomFetch from "../lib/LagomFetch";

class FetchContainer extends React.Component {
  constructor() {
    super();
    this.state = {
      object: null,
    };
  }

  componentDidMount() {
    lagomFetch(this.props.method, this.props.url, this.props.data, (resp) => {
      var current_state = this.state;
      current_state.object = resp;
      this.setState(current_state);
    });
  }

  render() {
    return (
      <table>
        <tr>
          <td>
            <strong>Method</strong>
          </td>
          <td>{this.props.method}</td>
        </tr>
        <tr>
          <td>
            <strong>URL</strong>
          </td>
          <td>{this.props.url}</td>
        </tr>
        <tr>
          <td>
            <strong>Request payload</strong>
          </td>
          <td>{JSON.stringify(this.props.data)}</td>
        </tr>
        <tr>
          <td>
            <strong>Response</strong>
          </td>
          <td>{JSON.stringify(this.state.object)}</td>
        </tr>
      </table>
    );
  }
}

const payload_data = require("./files.json");
PayloadRegister.enable(true);
PayloadRegister.registerBatchPayloads(payload_data);

export default {
  title: "Payload files",
  component: () => <div></div>,
};

export const basic_get = () => <FetchContainer url="/my-url-3" method="GET" />;
export const basic_post = () => (
  <FetchContainer url="/my-url-3" method="POST" />
);
export const post_with_param = () => (
  <FetchContainer url="/my-url-3" method="POST" data={{ param1: "test" }} />
);
