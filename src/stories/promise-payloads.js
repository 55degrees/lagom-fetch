import React from "react";

import PayloadRegister from "../lib/PayloadRegister";
import lagomFetch from "../lib/LagomFetch";

class FetchContainer extends React.Component {
  constructor() {
    super();
    this.state = {
      object: null,
    };
  }

  async loadData() {
    var current_state = this.state;
    current_state.object = await lagomFetch(
      this.props.method,
      this.props.url,
      this.props.data
    );
    this.setState(current_state);
  }

  componentDidMount() {
    this.loadData();
  }

  render() {
    return (
      <table>
        <tr>
          <td>
            <strong>Method</strong>
          </td>
          <td>{this.props.method}</td>
        </tr>
        <tr>
          <td>
            <strong>URL</strong>
          </td>
          <td>{this.props.url}</td>
        </tr>
        <tr>
          <td>
            <strong>Request payload</strong>
          </td>
          <td>{JSON.stringify(this.props.data)}</td>
        </tr>
        <tr>
          <td>
            <strong>Response</strong>
          </td>
          <td>{JSON.stringify(this.state.object)}</td>
        </tr>
      </table>
    );
  }
}

PayloadRegister.enable(true);
PayloadRegister.registerPayload("GET", "/my-url-5", null, "string contents");

export default {
  title: "Promise interface",
  component: () => <div></div>,
};

export const basic_get = () => <FetchContainer url="/my-url-5" method="GET" />;
