import React from "react";

import PayloadRegister from "../lib/PayloadRegister";
import lagomFetch from "../lib/LagomFetch";

class FetchContainer extends React.Component {
  constructor() {
    super();
    this.state = {
      object: null,
    };
  }

  componentDidMount() {
    lagomFetch(this.props.method, this.props.url, this.props.data, (resp) => {
      var current_state = this.state;
      current_state.object = resp;
      this.setState(current_state);
    });
  }

  render() {
    return (
      <table>
        <tr>
          <td>
            <strong>Method</strong>
          </td>
          <td>{this.props.method}</td>
        </tr>
        <tr>
          <td>
            <strong>URL</strong>
          </td>
          <td>{this.props.url}</td>
        </tr>
        <tr>
          <td>
            <strong>Request payload</strong>
          </td>
          <td>{JSON.stringify(this.props.data)}</td>
        </tr>
        <tr>
          <td>
            <strong>Response</strong>
          </td>
          <td>{this.state.object}</td>
        </tr>
      </table>
    );
  }
}

PayloadRegister.enable(true);
PayloadRegister.registerPayload("GET", "/my-url-1", null, "string contents");
PayloadRegister.registerPayload(
  "POST",
  "/my-url-1",
  null,
  "this is a post with no contents"
);
PayloadRegister.registerPayload(
  "POST",
  "/my-url-2",
  { param1: "test" },
  "this is a post with no contents"
);

export default {
  title: "Payloads",
  component: () => <div></div>,
};

export const basic_get = () => <FetchContainer url="/my-url-1" method="GET" />;
export const basic_post = () => (
  <FetchContainer url="/my-url-1" method="POST" />
);
export const post_with_param = () => (
  <FetchContainer url="/my-url-2" method="POST" data={{ param1: "test" }} />
);
