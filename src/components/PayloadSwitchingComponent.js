import { PayloadRegister } from "../lib/index";
import React from "react";

class PayloadSwitchingComponent extends React.Component {
  componentDidMount() {
    if (this.props.mode) {
      PayloadRegister.setPayloadMode(this.props.mode);
    } else {
      PayloadRegister.setPayloadMode(null);
    }
  }

  render() {
    return this.props.children;
  }
}

export default PayloadSwitchingComponent;
