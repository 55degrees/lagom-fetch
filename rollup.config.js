// rollup.config.js

import babel from "rollup-plugin-babel";
import postcss from "rollup-plugin-postcss";

const config = {
  plugins: [
    postcss({
      extensions: [".css"],
    }),
    babel({
      exclude: "node_modules/**",
    }),
  ],

  input: "src/lib/index.js",
  external: ["react"],
  output: {
    exports: "named",
    format: "cjs",
    name: "lagom-fetch",
    globals: {
      react: "React",
    },
  },
};
export default config;
