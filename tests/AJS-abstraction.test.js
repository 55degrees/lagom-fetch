import payloadRegister from "../src/lib/PayloadRegister";
import lagomFetch from "../src/lib/LagomFetch";

beforeAll(() => {
  global.window = {
    AJS: {
	  contextPath: function() { return ''},
      $: {
        ajax: function (opts) {
          opts.success();
        },
      },
    },
  };

  payloadRegister.enable(false);
});

test("verify that AJS request is called in an AJS environment", (done) => {
  lagomFetch("GET", "nowhere", null, null, (data) => {
    done();
  });
});
