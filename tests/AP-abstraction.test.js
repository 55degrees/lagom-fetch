import payloadRegister from "../src/lib/PayloadRegister";
import lagomFetch from "../src/lib/LagomFetch";

beforeAll(() => {
  global.window = {
    AP: {
      request: function (opts) {
        opts.success();
      },
    },
  };

  payloadRegister.enable(false);
});

test("verify that AP.request is called in an AP environment", (done) => {
  lagomFetch("GET", "nowhere", null, null, (data) => {
    done();
  });
});
