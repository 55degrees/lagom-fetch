import payloadRegister from "../src/lib/PayloadRegister";
import lagomFetch from "../src/lib/LagomFetch";

beforeAll(() => {
  global.window = {};

  payloadRegister.enable(true);

  payloadRegister.registerPayload("GET", "/get-url", null, { key: "a" });
  payloadRegister.registerPayload("POST", "/post-url", null, { key: "b" });
  payloadRegister.registerPayload(
    "POST",
    "/post-specific-url",
    { x: "a" },
    { key: "c" }
  );
  payloadRegister.registerPayload(
    "POST",
    "/post-specific-url",
    { x: "b" },
    { key: "d" }
  );

  // test the bulk additions
  var payloads = {};

  for (var x = 0; x < 10; x++) {
    payloads["bulk-" + x] = {
      method: "GET",
      url: "/bulk-created-" + x,
      body: null,
      response: { key: "something " + x },
    };
  }
  payloadRegister.registerBatchPayloads(payloads);
});

test("check the bulkPayload url", (done) => {
  lagomFetch("GET", "/bulk-created-5").then((data) => {
    expect(data.key).toBe("something 5");
    done();
  });
});

test("get nothing back when the url hasnt been mapped", (done) => {
  lagomFetch("GET", "this-isnt -mapped", null)
    .then((data) => {
      expect(data).toEqual({ status: 404 });
      done();
    })
    .catch();
});

test("get nothing back when the url hasnt been mapped -cb ", (done) => {
  lagomFetch("GET", "this-isnt -mapped", null, null, (_err, data) => {
    expect(data).toEqual({ status: 404 });
    done();
  });
});

test("test basic get of known url", (done) => {
  lagomFetch("GET", "/get-url").then((data) => {
    expect(data.key).toEqual("a");
    done();
  });
});

test("test basic get of known url - cb", (done) => {
  lagomFetch("GET", "/get-url", null, (_err, data) => {
    expect(data.key).toEqual("a");
    done();
  });
});

test("test basic post of known url", (done) => {
  lagomFetch("POST", "/post-url").then((data) => {
    expect(data.key).toEqual("b");
    done();
  });
});

test("test basic post of known url - cb", (done) => {
  lagomFetch("POST", "/post-url", null, (_err, data) => {
    expect(data.key).toEqual("b");
    done();
  });
});

test("test  post of known url with param 1", (done) => {
  lagomFetch("POST", "/post-specific-url", { x: "a" }).then((data) => {
    expect(data.key).toEqual("c");
    done();
  });
});

test("test  post of known url with param 1 -cb ", (done) => {
  lagomFetch("POST", "/post-specific-url", { x: "a" }, (_err, data) => {
    expect(data.key).toEqual("c");
    done();
  });
});

test("test  post of known url with param 2", (done) => {
  lagomFetch("POST", "/post-specific-url", { x: "b" }).then((data) => {
    expect(data.key).toBe("d");
    done();
  });
});

test("test  post of known url with param 2 -cb ", (done) => {
  lagomFetch("POST", "/post-specific-url", { x: "b" }, (_err, data) => {
    expect(data.key).toBe("d");
    done();
  });
});
