# What is lagom-fetch

LagomFetch is an abstraction layer that allows an Atlassian based app to use the same code to make requests in either Atlassian Connect javascript library (Cloud) or Atlassian p2 library - AJS (Server/Data Center). (Hoping to add Forge support later).

In addition to this - it allows you to mock payload loading for when you're using storybook or are loading pages outside of the Atlassian environment.

### Installation

Simply add:
`npm install @55degrees/lagom-fetch`

Then

```
import lagomFetch from '@55degrees/lagom-fetch';

lagomFetch(method, url, data, (err, resp)=>
    {
    // do stuff here
    })
```

or

```
lagomFetch(method, url, data).then(resp=>
{
    // do stuff here
})
```

Where:

- *method* is 'POST', 'GET', 'DELETE' etc.
- *url* is the url that you're calling (note that for server you do not include the context path) 
- *data* is null for GET and DELETE methods. For other objects it is javascript object to be sent as the body;

In return you get a javascript object of the return of the object. If there's an error - in callback mode there is an error object also returned.

## Mock data...

If you want to emulate the calls (like in a Storybook).

Simply import the PayloadRegister - by

```
import { PayloadRegister} from "@teamlagom/lagom-fetch";
```


Then enable it:

```
PayloadRegister.enable(true);
```

### Register a single payload

Simply call the registerPayload method with the method, the url, body-to-send and the response object).

For example:

```
PayloadRegister.registerPayload('POST','/my-url-1',null, "this is a post with no contents")
```

If you have a url that can respond differently based on the POST body, you can register the same url with a body-to-send object with the import parameters to differentate the request on. For example:

```
PayloadRegister.registerPayload('POST','/my-url-2',{param1:"test a"}, { a:1} )
PayloadRegister.registerPayload('POST','/my-url-2',{param1:"test b"}, {a:2} )
```

When lagomFetch makes a request with a body object of **param1**:'test a' to /my-url-2 - the response will be {a:1}.
If **param1**:'test b' is sent - then the response is {a:2}

### Bulk load urls

Got a lot of objects that you want to load? Then create a js hash (key is an id of the payload for future usage). The value of the object has the following key/val :
_method_: The method to mock
_url_: The url to respond to
_body_: The body match
_response_: The object that should be sent back.

### Delay

Want to emulate a delay on the response? Simply call:
`PayloadRegister.setRequestDelay(time) `
where time is the time in ms.
